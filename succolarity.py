from PIL import Image, ImageDraw
import moviepy.editor as mp
import numpy as np
from multiprocessing import Pool
from functools import partial
from multiprocessing import shared_memory
import multiprocessing as mpc
import os
import csv



def succolarity_v1(img, start=0, angle=0,  **kwargs):
    """
    path -str- scieżka zapisu
    name -str- nazwa pliku
    """
    path = kwargs.get('path', '')
    name = kwargs.get('name', 'image')+str(start)
    img_rotated = img.rotate(angle, expand=1)  # counterclockwise
    image = img_rotated.convert("RGB")
    pix = image.load()
    width, height = image.size

    # Checking right side fo the image
    for row in range(height):
        marker = "black"
        for col in range(start, width):
            # if col == start:
            #     pix[col, row] = (255, 0, 0)  # red scanning line
            if marker == "black":
                if pix[col, row] == (255, 255, 255):
                    marker = "white"
            else:
                pix[col, row] = (255, 255, 255)

    # Checking left side
    for row in range(height):
        marker = "black"
        for col in range(start, -1, -1):
            if marker == "black":
                if pix[col, row] == (255, 255, 255):
                    marker = "white"
            else:
                pix[col, row] = (255, 255, 255)

    img_reversed = image.rotate(-angle, expand=1)  # Back to default position

    if path:
        img_reversed.save(path + '/' + name + '_v1.png')

    return img_reversed


def succolarity_v2(img, start=0, angle=0, **kwargs):
    """
    path -str- scieżka zapisu
    name -str- nazwa pliku
    """

    path = kwargs.get('path', '')
    name = kwargs.get('name', 'image')+str(start)
    img_rotated = img.rotate(angle, expand=1)  # anticlockwise
    image = img_rotated.convert("RGB")
    pix = image.load()
    width, height = image.size

    for row in range(height):
        if pix[start, row] == (0, 0, 0):
            ImageDraw.floodfill(image, (start, row), (255, 0, 255))

    for row in range(height):
        for col in range(width):
            # if col == start:
            #     pix[col, row] = (255, 0, 0)  # red scanning line
            #     # continue
            if pix[col, row] == (255, 255, 255) or pix[col, row] == (0, 0, 0):
                pix[col, row] = (255, 255, 255)
            elif pix[col, row] == (255, 0, 255):
                pix[col, row] = 0

    img_reversed = image.rotate(-angle, expand=1)

    if path:
        img_reversed.save(path + '/' + name + '_v2.png')

    return img_reversed


def generate_video(image, path: str, fun, option):
    """Create video from image you want to be analyzed"""

    if option == "vertical":
        rng = image.height
        ang = 90
    elif option == "horizontal":
        rng = image.width
        ang = 0
    else:
        raise AttributeError('Wrong option')

    if not os.path.exists(os.path.join(path,'videos')):
        os.mkdir(os.path.join(path,'videos'))

    fn = fun.__name__.split('_')[1]
    # multiprocessing part
    p = Pool()  # default cpu_count
    partial_func = partial(fun, image, angle=ang, name=option, path=path)
    frames = list(p.map(partial_func, range(rng)))
    p.close()
    p.join()

    # drawing red scanning line
    for i in range(rng):
        if option == "vertical":
            ImageDraw.Draw(frames[i]).line([0, i, image.width, i], fill=(255, 0, 0))
        elif option == "horizontal":
            ImageDraw.Draw(frames[i]).line([i, 0, i, image.height], fill=(255, 0, 0))

    # Rewind animation
    rev = frames[:]
    rev.reverse()
    frames = frames + rev
    gif = Image.new('RGB', image.size)
    # Supported extensions .gif, .pdf, .webp, .tiff, .apng

    gif.save(os.path.join(path,'videos') + '/' + option + '_scanning_' + fn + '.gif', save_all=True, append_images=frames, duration=50)
    clip = mp.VideoFileClip((os.path.join(path,'videos') + '/' + option + '_scanning_' + fn + '.gif'))
    clip.write_videofile((os.path.join(path,'videos') + '/' + option + '_scanning_' + fn + '.mp4'))


def calculate_box(shape,type,name,corners : tuple): # Corners should be passed on as follows:
    # top left, top right, bottom left, bottom right

    current_row = []
    shared_mem = shared_memory.SharedMemory(name=name)
    img_copy = np.ndarray(shape, dtype=type, buffer=shared_mem.buf)

    pix_amount = ((corners[2][1] - corners[0][1]) + 1)**2

    for index,column in enumerate(range(corners[0][1],corners[2][1] + 1)):
        for row in range(corners[0][0],corners[1][0] + 1):

            current_row.append((row,column))

        if index == 0:
            coordinates = np.array(current_row)
        else:
            coordinates = np.vstack((coordinates,current_row))
        current_row = []

    pressure = corners[0][1] + (corners[2][1] - corners[0][1] + 1)/2
    box_sum = 0
    box_max_value = 0

    for row in coordinates:

        if (img_copy[int(row[1]),int(row[0])] == [0,0,0]).any():
            box_sum += 1
        box_max_value += 1

    sigma = box_sum/pix_amount * pressure
    sigma_max = box_max_value/pix_amount * pressure

    shared_mem.unlink()
    shared_mem.close()

    return sigma,sigma_max


def succolarity_value(im):

    width, height = im.size

    if width != height:
        raise AttributeError('Image must be square')

    image_array = np.array(im)
    shared_mem = shared_memory.SharedMemory(create=True, size=image_array.nbytes)
    shared_copy = np.ndarray(image_array.shape, dtype=image_array.dtype, buffer=shared_mem.buf)
    shared_copy[:] = image_array[:]

    succ = {}


    divisors = []
    for i in range(1,width):
        if width % i == 0:
            divisors.append(i)

    for div in divisors:

        corners = []

        for row in range(int(width/div)):
            for column in range(int(height/div)):
                corners.append(((div*row,column*div),(div*row + div-1,column*div),
                                (div*row,column*div + div - 1),(div*row + div-1,column*div + div - 1)))

        boxes_sum = 0
        max_sum = 0

        for cor in corners:
            sigma,sigmamax = calculate_box(image_array.shape,image_array.dtype,shared_mem.name,cor)
            boxes_sum += sigma
            max_sum += sigmamax

        succ[div] = boxes_sum/max_sum
    shared_mem.unlink()
    shared_mem.close()
    return succ  # Returns a dict, where each key is a possible divisor, values are corresponding succolarity values

def cropToSquare(image):

    width, height = image.size

    if width > height:
        dif = width - height
        corr = int(dif / 2)
        image = image.crop((corr, 0, width - corr, height))

    elif height > width:
        dif = height - width
        corr = int(dif / 2)
        image = image.crop((0, corr, width, height - corr))

    return image


def processImages(dir_path:str):

    results_path = os.path.join(dir_path,'results')
    results_filename = 'results.csv'
    supported_formats = ['.png','.jpg','.jpeg']  # Nwm czy to wgl potrzebne w sumie, ale uznałem, że dodam

    if not os.path.exists(results_path):
        os.mkdir(results_path)

    content = os.listdir(dir_path)
    valid_images = [img for img in content if True in (extension in img for extension in supported_formats)]

    for index,file in enumerate(valid_images):
        valid_images[index] = os.path.join(dir_path,file)

    print(valid_images)
    p = mpc.Pool()
    func = partial(processPic,results_path)
    res = p.imap(func,(image for image in valid_images))
    p.close()
    p.join()

    with open(os.path.join(results_path,results_filename),'w',newline='\n') as results_file:
        header_flag = True
        header = ['filename','orientation']
        writer = csv.writer(results_file,delimiter=';')
        for r in res:
            for row in r:
                if header_flag:
                    columns_count = len(row) - 2
                    for i in range(columns_count):
                        header.append('div' + str(i+1))
                        header.append('succolarity_value' + str(i+1))
                    writer.writerow(header)
                    header_flag = False
                writer.writerow(row)


def processPic(results_path,img_path):

    res = []
    image = Image.open(img_path, 'r')
    image = cropToSquare(image)

    directions = [('t2b', 90), ('r2l', 180), ('b2t', 270), ('l2r', 360)]

    for orientation, angle in directions:
        row = [img_path,orientation]

        if orientation == 'r2l' or orientation == 'b2t':
            img_rotated = image.rotate(angle + 90, expand=1)
        else:
            img_rotated = image.rotate(angle-90,expand=1)

        results = succolarity_value(img_rotated)
        for divisor in results:
            row.append(divisor)
            row.append(results[divisor])

        res.append(row)
    return res
