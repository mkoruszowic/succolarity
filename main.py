from PIL import Image
import os
import time
import succolarity as sc


start = time.perf_counter()
cwd = os.getcwd()

#main part
# for file in os.listdir():
#     if file.endswith('.png'):
#         name = file.split('.')[0]
#         image = Image.open(file, 'r')
#         dir = os.path.join(cwd, name)
#
#         # If file already exist image won't be analyzed
#         if not os.path.exists(dir):
#             os.mkdir(dir)
#             for fun in (sc.succolarity_v1, sc.succolarity_v2):
#                 method = fun.__name__.split('_')[1]
#                 for option in ("horizontal", "vertical"):
#                     sc.generate_video(image, path=name, fun=fun, option=option)

# testing part
file = 'test2.png'
name = file.split('.')[0]
image = Image.open(file, 'r')
dir = os.path.join(cwd, name)

if not os.path.exists(dir):
    os.mkdir(dir)
    for fun in (sc.succolarity_v1, sc.succolarity_v2):
        method = fun.__name__.split('_')[1]
        if not os.path.exists(os.path.join(dir,method)):
            os.mkdir(os.path.join(dir,method))
        for option in ("horizontal", "vertical"):
            sc.generate_video(image, path=os.path.join(dir,method), fun=fun, option=option)

    print('Elapsed time: {:.3g}'.format((time.perf_counter() - start)/60), 'min')
    sc.processImages(os.path.join(dir,'v2'))
